# Getting Started with React

Install `create-react-app`:

    npm i -g create-react-app


Create a React app:

    create-react-app my-first-app
    cd my-first app

Run in a local dev env:

    npm start

Deploy:

    npm run build

